""" 
    Flask app module
"""
from json import dumps

from flask import Flask

from aurora_swap import aurora_auroraswap_get_apr

app = Flask(__name__)

@app.route("/get-near-weth-yearly-apr")
def get_near_weth_yearly_apr_api():
    response = aurora_auroraswap_get_apr("NEAR-WETH")
    return dumps(response)
