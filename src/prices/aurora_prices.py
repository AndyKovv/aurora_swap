"""
    Aurora prices module
"""
import requests
from logger import _debug
from config import AURORA_TOKENS, GET_PRICE_REQUEST_URL

def look_up_aurora_prices(prices_ids):
    """
        Method should get look up aurora prices
    """
    prices = {}
    join_ = '%2C'

    formatted_ids = join_.join(prices_ids)
    _debug(f'look_up_aurora_prices formatted_ids: {formatted_ids}')

    url = GET_PRICE_REQUEST_URL.format(formatted_ids)

    response = requests.get(url)
    if response.status_code != 200:
        # Can be added custom exception
        raise Exception(f"look_up_aurora_prices error get prices: code: {response.status_code}")

    data = response.json()

    for k, v in data.items():
        if v.get("usd"):
            prices[k] = v

    return prices


def get_aurora_prices():
    """
        Function should get aurora prices
    """
    prices = {}

    prices_ids = [price['id'] for price in AURORA_TOKENS]
    aurora_prices_map = look_up_aurora_prices(prices_ids)

    for aurora_token in AURORA_TOKENS:
        aurora_price = aurora_prices_map.get(aurora_token.get('id'))
        if aurora_price:
            prices.update({
                aurora_token.get('contract'): aurora_price
            })
    _debug(f'Prices are: prices: {prices}')
    return prices
