"""
    Logger module
"""
import logging

SYSTEM_LOG_LEVEL = 'DEBUG'

FORMAT = '%(asctime)s %(message)s'

def get_console_handler():
    """
        Function should get console handler
    """
    formatter = logging.Formatter(fmt=FORMAT)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(SYSTEM_LOG_LEVEL)
    console_handler.setFormatter(formatter)
    return console_handler

def _debug_logger():
    console_handler = get_console_handler()
    logger = logging.Logger('aurora_debug_logger')
    logger.setLevel('DEBUG')
    logger.addHandler(console_handler)
    return logger.debug

_debug = _debug_logger()