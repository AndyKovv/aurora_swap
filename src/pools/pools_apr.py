"""
    APR for pools
"""
from logger import _debug
from pools.aurora_pool_info import get_price


def get_apr_for_pool(prices, pool_price, pool_info, rewards_per_week, total_alloc_points, reward_token_address):
    """
        Function should get apr for pool
    """
    pool_rewards_per_week = pool_info.get("alloc_points") / total_alloc_points * rewards_per_week
    reward_price = get_price(reward_token_address, prices)
    if not reward_price:
        _debug(f'get_apr_for_pool: Price not found for {reward_token_address}')
        return None

    staked_tvl = pool_price.get("staked_tvl")
    if not staked_tvl:
        _debug(f'get_apr_for_pool: staked_tvl found for {pool_price}')
        return 0

    usd_per_week = pool_rewards_per_week * reward_price.get("usd")
    weekly_apr = usd_per_week / staked_tvl * 100
    yearly_apr = weekly_apr * 52
    t0 = pool_price.get('t0')
    t1 = pool_price.get('t1')

    if t0 and t1:
        _debug(f'Name: {t0.get("symbol")}-{t1.get("symbol")}')

    _debug(f'Yearly apr {yearly_apr}')

    # Here we can add handler for handle different names
    name = f'{t0.get("symbol")}-{t1.get("symbol")}'
    return (name, round(yearly_apr, 2))


def get_apr_for_pools(
    pool_counts, prices, pool_prices, pool_infos, total_alloc_points,
    rewards_per_week, reward_token_address, chosen_pool_name
):
    """
        Function should get apr for pools
    """
    name_to_apr = {}
    for pool_idx in range(0, pool_counts):
        if pool_idx < len(pool_prices):
            pool_price = pool_prices[pool_idx]
            pool_info = pool_infos[pool_idx]
            name, yearly_apr = get_apr_for_pool(
                prices, pool_price, pool_info,
                rewards_per_week, total_alloc_points, reward_token_address
            )
            if not chosen_pool_name:
                name_to_apr.update({name: {'yearly_apr': yearly_apr}})

            if chosen_pool_name == name:
                return {name: {'yearly_apr': yearly_apr}}

    return name_to_apr
