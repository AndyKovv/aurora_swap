"""
    Get aurora pool info module
"""
from web3 import Web3
from logger import _debug
from tokens.aurora_token import get_aurora_token
from helpers import convert_output_to_dict
from config import ALLOWED_POOLS, YOUR_ADDRESS, NEAR_WETH_POOL_INDEXES, USE_ALL_POOLS



def get_pool_infos(w3_connection, main_contract, main_address, pools_list):
    """
        Function should get information for pools
    """
    # pending reward function 'pendingBRL'

    your_address = Web3.toChecksumAddress(YOUR_ADDRESS)
    pool_infos = []
    pool_idexes = range(0, pools_list)

    if not USE_ALL_POOLS and NEAR_WETH_POOL_INDEXES:
        pool_idexes = NEAR_WETH_POOL_INDEXES

    for pool_idx in pool_idexes:
        pool_info = convert_output_to_dict("poolInfo", main_contract.functions.poolInfo(pool_idx).call())

        _debug(f"Single Pool info: {pool_info}")

        # Prevent walk through all known pools
        if pool_info["lpToken"] not in ALLOWED_POOLS:
            continue

        if pool_info["allocPoint"] == 0:
            pool_infos.append(
                {
                    'address': pool_info.get("lpToken"),
                    'allocPoints': pool_info.get("allocPoint") or 1,
                    'poolToken': None,
                    'userStaked' : 0,
                    'pendingRewardTokens' : 0,
                }
            )
        token_address = pool_info.get("lpToken") or pool_info.get("token") or pool_info.get("stakingToken")
        pool_token = get_aurora_token(w3_connection, token_address, main_address)
        user_info = convert_output_to_dict("userInfo", main_contract.functions.userInfo(pool_idx, your_address).call())
        pending_reward_tokens = main_contract.functions.pendingBRL(pool_idx, your_address).call()
        staked = user_info.get("amount") / 10 ** pool_token.get("decimals")
        pool_infos.append({
            "address" : pool_info.get("lpToken") or pool_info.get("token") or pool_info.get("stakingToken"),
            "alloc_points": pool_info.get("allocPoint") or 1,
            "pool_token": pool_token,
            "userStaked" : staked,
            "pendingRewardTokens" : pending_reward_tokens / 10 ** 18,
            "depositFee" : (pool_info.get("depositFeeBP") or 0) / 100,
            "withdrawFee" : (pool_info.get("withdrawFeeBP") or 0) / 100
        })
    return pool_infos


def get_price(token, prices):
    """
        Function should get price
    """

    price = {}

    for k, v in prices.items():
        if k.lower() == token.lower():
            price = v
            break

    return price

def get_aurora_erc20_price(pool_token, prices):
    """
        Function should get aurora pool prices
    """

    price = get_price(pool_token.get("address"), prices)
    price_usd = price.get("usd", 0)
    tvl = pool_token.get("total_supply", 0) * price_usd / 10 ** pool_token.get("decimals")
    staked_tvl = pool_token.get("staked") * price_usd

    return {
        'staked_tvl' : staked_tvl,
        'price' : price_usd,
        'stake_token_ticker' : pool_token.get("symbol"),
        'tvl' : tvl,
    }

def get_aurora_pool_uni_price(tokens, pool_token, prices):
    """
        Function should get aurora uni price
    """
    t0 = get_price(pool_token.get("token0"), tokens)
    p0 = get_price(pool_token.get("token0"), prices).get("usd")

    t1 = get_price(pool_token.get("token1"), tokens)
    p1 = get_price(pool_token.get("token1"), prices).get("usd")

    if p0 is None and p1 is None:
        return

    q0 = pool_token.get("q0") / 10 ** t0.get("decimals")
    q1 = pool_token.get("q1") / 10 ** t1.get("decimals")

    if p0 is None:

        _debug(f'***p0: {pool_token.get("token0")}')

        p0 = q1 * p1 / q0
        prices[pool_token.get("token0")] = {"usd" : p0}

    if p1 is None:

        _debug(f'***p1: {pool_token.get("token1")}')

        p1 = q0 * p0 / q1
        prices[pool_token.get("token1")] = {"usd" : p1}

    tvl = q0 * p0 + q1 * p1

    price = tvl / pool_token.get("total_supply")
    prices[pool_token.get("address")] = {"usd" : price}
    staked_tvl = pool_token.get("staked") * price

    return {
        't0': t0,
        'p0': p0,
        'q0': q0,
        't1': t1,
        'p1': p1,
        'q1'  : q1,
        'price': price,
        'tvl' : tvl,
        'staked_tvl' : staked_tvl,
    }


def get_pool_prices(tokens, pool_infos, prices):
    """
        Function should get poll prices
    """
    pool_prices = []
    for pool_info in pool_infos:
        pool_token = pool_info.get("pool_token")
        if pool_token:
            _debug(f'get_pool_prices pool_token: {pool_token}')
            if pool_token.get('token0'):
                pool_price = get_aurora_pool_uni_price(tokens, pool_token, prices)
                pool_prices.append(pool_price)
                continue

            pool_price = get_aurora_erc20_price(pool_token, prices)
            pool_prices.append(pool_price)


    return pool_prices
