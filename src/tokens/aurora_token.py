from web3 import Web3
from logger import _debug

from config_abi import UNI_ABI, ERC20_ABI
from config import YOUR_ADDRESS


def _make_contract(w3_connection, address, abi):
    """
        Function should make contract
    """
    return w3_connection.eth.contract(
        address=address,
        abi=abi
    )


your_address = Web3.toChecksumAddress(YOUR_ADDRESS)
def get_aurora_erc_20(erc_20_contract, token_address, staking_address):
    """
        Function should get aurora erc token
    """
    decimals = erc_20_contract.functions.decimals().call()
    staked = erc_20_contract.functions.balanceOf(staking_address).call()
    unstaked = erc_20_contract.functions.balanceOf(your_address).call()
    name = erc_20_contract.functions.name().call()
    symbol = erc_20_contract.functions.symbol().call()
    total_supply = erc_20_contract.functions.totalSupply().call()
    return {
        'address': token_address,
        'name': name,
        'symbol': symbol,
        'total_supply': total_supply,
        'decimals': decimals,
        'staked': staked / 10 ** decimals,
        'unstaked': unstaked / 10 ** decimals,
        'tokens': [token_address],
    }


def get_aurora_uni_pool(w3_connection, uni_contract, token_address, staking_address):
    """
        Function shold get aurota uni pool
    """
    symbol = uni_contract.functions.symbol().call()
    decimals = uni_contract.functions.decimals().call()
    token0 = uni_contract.functions.token0().call()
    token1 = uni_contract.functions.token1().call()
    name = uni_contract.functions.name().call()
    total_supply = uni_contract.functions.totalSupply().call()
    staked = uni_contract.functions.balanceOf(staking_address).call()
    unstaked = uni_contract.functions.balanceOf(your_address).call()

    q0 = None
    q1 = None
    is1inch = None

    try:
        if token0 == "0x0000000000000000000000000000000000000000":
            q0 = w3_connection.get_balance(token_address)

        else:
            c0 = _make_contract(w3_connection, token0, ERC20_ABI)
            q0 = c0.functions.balanceOf(token_address).call()

        if token1 == "0x0000000000000000000000000000000000000000":
            q1 = w3_connection.get_balance(token_address)

        else:
            c1 = _make_contract(w3_connection, token1, ERC20_ABI)
            q1 = c1.functions.balanceOf(token_address).call()

        is1inch = True
    except Exception:
        # Have to add exception handler
        pass

    return {
        'symbol': symbol,
        'name': name,
        'address': token_address,
        'token0': token0,
        'q0': q0,
        'token1': token1,
        'q1': q1,
        'total_supply': total_supply / 10 ** decimals,
        'staking_address': staking_address,
        'staked': staked / 10 ** decimals,
        'decimals': decimals,
        'unstaked': unstaked / 10 ** decimals,
        'tokens' : [token0, token1],
        'is1inch': is1inch,
    }


def get_aurora_token(w3_connection, token_address, staking_address):
    """
        Function should get aurora token
    """
    try:
        uni_contract = _make_contract(w3_connection, token_address, UNI_ABI)
        uni_contract.functions.token0().call()
        return get_aurora_uni_pool(w3_connection, uni_contract, token_address, staking_address)
    except Exception:
        pass

    erc_20_contract = _make_contract(w3_connection, token_address, ERC20_ABI)
    _name = erc_20_contract.functions.name().call()
    erc20_data = get_aurora_erc_20(erc_20_contract, token_address, staking_address)
    return erc20_data


def map_token_addresses(poll_infos):
    """
        Function should map token addresses and
        return token adddresses
    """
    token_addresses = []
    for pool_info in poll_infos:
        pool_token = pool_info.get("pool_token")
        if not pool_token:
            continue
        token_addresses.extend(pool_token.get("tokens", []))

    return token_addresses


def map_to_aurora_token_addresses(w3_connection, token_addresses, staking_address):
    """
        Function should map to aurora token addresses
    """
    token_address_to_aurora_address = {}
    for token_address in set(token_addresses):
        _debug(f'Get aurora token for token_address: {token_address}, staking_address: {staking_address}')

        aurora_token = get_aurora_token(
            w3_connection, token_address, staking_address
        )
        if not aurora_token:
            continue

        token_address_to_aurora_address[token_address] = aurora_token

    _debug(f'Map aurora token address completed: {token_addresses}')
    return token_address_to_aurora_address
