
PREPARED_ABI = None

def prepare_abi_for_parse_output(abi):

    global PREPARED_ABI
    data = {}
    for abi_obj in abi:
        if abi_obj['type'] != 'function':
            continue

        data.update({abi_obj['name']: abi_obj})

    PREPARED_ABI = data


def convert_output_to_dict(
    function_name, function_output
):
    """
        Function should conver output to dict
    """

    function = PREPARED_ABI.get(function_name)
    result = {}
    for idx, output_field in enumerate(function['outputs']):
        result.update({output_field['name']: function_output[idx]})

    return result


