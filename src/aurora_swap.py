
from web3 import Web3
from json import dumps

from logger import _debug
from tokens.aurora_token import (
    get_aurora_token, map_token_addresses, map_to_aurora_token_addresses
)
from pools.aurora_pool_info import get_pool_infos, get_pool_prices
from prices.aurora_prices import get_aurora_prices
from helpers import prepare_abi_for_parse_output

from pools.pools_apr import get_apr_for_pools
from config import WEB3_HTTP_PROVIDER_URL, BRL_CHEF_ADDR
from config_abi import BRL_CHEF_ABI


prepare_abi_for_parse_output(BRL_CHEF_ABI)


def aurora_auroraswap_get_apr(apr_pool_name):
    """
        Function should get aurora swap get arp
    """
    w3 = Web3(Web3.HTTPProvider(WEB3_HTTP_PROVIDER_URL))
    contract = w3.eth.contract(
        address=BRL_CHEF_ADDR,
        abi=dumps(BRL_CHEF_ABI)
    )

    current_block = w3.eth.get_block_number()
    _debug(f'Current block: {current_block}')

    multiplier = contract.functions.getMultiplier(current_block, current_block+1).call()
    _debug(f'Multiplier is: {multiplier}')

    rewards_per_block = contract.functions.BRLPerBlock().call()
    _debug(f'Rewards per block {rewards_per_block}')

    rewards_per_week = rewards_per_block /1e18 * multiplier * 604800 / 1.1;
    _debug(f'Rewards per week: {rewards_per_week}')

    prices = get_aurora_prices()

    pool_counts = contract.functions.poolLength().call()
    _debug(f'Pool count: {pool_counts}')

    total_alloc_points = contract.functions.totalAllocPoint().call()
    _debug(f'Total alloc points {total_alloc_points}')

    reward_token_address = contract.functions.BRL().call()
    _debug(f'Rewart token address {reward_token_address}')

    pool_infos = get_pool_infos(w3, contract, BRL_CHEF_ADDR, pool_counts)
    _debug(f"Pools infos {pool_infos}, {len(pool_infos)}")

    pool_counts = len(pool_infos)

    token_addresses = map_token_addresses(pool_infos)
    _debug(f'Mapped token addresses {token_addresses}')

    tokens = map_to_aurora_token_addresses(w3, token_addresses, BRL_CHEF_ADDR)
    _debug(f'Aurora tokens {tokens}')

    pool_prices = get_pool_prices(tokens, pool_infos, prices)
    _debug(f'Aurora pool prices {pool_prices}')

    apr_for_pools = get_apr_for_pools(
        pool_counts, prices, pool_prices, pool_infos, total_alloc_points,
        rewards_per_week, reward_token_address, apr_pool_name
    )
    return apr_for_pools

# print(aurora_auroraswap_get_apr("NEAR-WETH"))
